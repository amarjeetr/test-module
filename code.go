package test_module

func Multiply(a, b int) int {
	return a * b
}

func Sub(a, b int) int {
	return a - b
}

func SubMany(a ...int) int {
	var sum int
	for _, v := range a {
		sum -= v
	}
	return sum
}
